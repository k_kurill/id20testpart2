<?php

namespace app\models;

use yii\base\Model;
use yii\db\Expression;
use app\models\Transaction;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;



/**
 * TransactionSearch represents the model behind the search form about `app\models\Transaction`.
 */
class TransactionSearch extends Transaction {

    public $year, $month;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['year', 'month'], 'integer'],
            [['card_number'], 'safe'],
        ];
    }
    
    public static function getCountList(){
        $data = self::find()
                ->select([
                    new Expression('YEAR(`date`) as year'),
                    new Expression('MONTH(`date`) as month'),
                    new Expression('count(*) as count'),
                ])
                ->groupBy([new Expression('YEAR(`date`)'), new Expression('MONTH(`date`)')])
                ->orderBy(['date'=>SORT_DESC])
                ->asArray()
                ->all();
        return self::getMenuData($data);
    }
    
    private static function getMenuData($data){
        $result = [];
        //Fill years menu items
        foreach($data as $row){
            $yearRecord = &$result[$row['year']];            
            //update count if record already exist or create if not exist
            $yearRecord['count'] = isset($yearRecord['count'])?$yearRecord['count']+$row['count']:$row['count'];
            $yearRecord['url'] = '#'; 
            //Fill month menu subitems
            if(!isset($yearRecord['items'][$row['month']])){
                $yearRecord['items'][$row['month']]['count']=0;
            }
            $yearRecord['items'][$row['month']]['count']+=$row['count'];
                
        }
        foreach($result as $key=>$res){
            //Making labels foreach year menu item
            $result[$key]['label'] = $key . ' (' . $res['count'].')';
            //Making labels foreach year menu subitem
            foreach($res['items'] as $month => $item){
                $label = \Yii::$app->formatter->asFuzzyDate(['y' => null,'m' => $month,'d' => null], 'MMMM') . ' (' . $item['count'] . ')';
                $result[$key]['items'][$month] = ['label'=>$label, 'url'=>'#'];
            }
        }
        return $result;
    }

    public static function getYearsList(){
        $years = self::find()->select([new Expression('YEAR(`date`) as year')])->distinct()->column();
        return array_combine($years,$years);
    }
    
    public static function getMonthList($year){
        $monthsQuery = self::find()
                ->select([new Expression('MONTH(`date`) as month')])
                ->distinct();
        if($year){
            $monthsQuery->andWhere('YEAR(`date`)=:year',[':year' => $year]);
        }
        $months = $monthsQuery->column();
        return array_combine($months,$months);
    }
    

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Transaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if($this->year){
            $query->andWhere('YEAR(`date`)=:year',[':year' => $this->year]);
        }
        if($this->month){
            $query->andWhere('MONTH(`date`)=:month',[':month' => $this->month]);
        }

        $query->andFilterWhere(['like', 'card_number', $this->card_number]);

        return $dataProvider;
    }

}
