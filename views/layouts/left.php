<?php
use app\models\TransactionSearch;

$transactionItems = TransactionSearch::getCountList();
$header = ['label' => 'Transactions', 'options' => ['class' => 'header']];
array_unshift($transactionItems, $header);

?>
<aside class="main-sidebar">

    <section class="sidebar">
        
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => $transactionItems
            ]
        ) ?>
    </section>
</aside>
