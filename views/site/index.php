<?php

use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transactions';
$this->params['breadcrumbs'][] = $this->title;


$yearDropdowm = Html::activeDropDownList($searchModel, 'year', $searchModel::getYearsList(), ['id'=>'year-dropd','class'=>'form-control','prompt'=>'Year']);

$monthDropdowm = Html::activeDropDownList($searchModel, 'month', $searchModel::getMonthList($searchModel->year), ['id'=>'month-dropd','class'=>'form-control','prompt'=>'Month']);


?>

<div class="box box-primary">
        <div class="box-header">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>    

            <?=GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id' => [
                        'attribute' => 'id',
                        'filter' => false,
                    ],
                    'card_number',
                    'date'=>[
                        'attribute' => 'date',
                        'filter' => '<div class="row">
                                        <div class="col-xs-6">'.$yearDropdowm.'</div>
                                        <div class="col-xs-6">'.$monthDropdowm.'</div>
                                    </div>',
                    ],
                    'volume' => [
                        'attribute' => 'volume',
                        'filter' => false,
                    ],
                    'service' => [
                        'attribute' => 'service',
                        'filter' => false,
                    ],
                // 'address_id',
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>
    </div>
</div>
