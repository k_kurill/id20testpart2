<?php

use yii\db\Migration;

class m161106_102802_init extends Migration {

    public function up() {
        $sql = file_get_contents(Yii::getAlias('@app/migrations/data/dump.sql'));
        $this->execute($sql);
    }

    public function down() {
        echo "m161106_102802_init cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
